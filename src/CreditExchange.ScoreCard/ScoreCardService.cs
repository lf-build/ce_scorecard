﻿using System;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.DataAttributes;
using CreditExchange.ScoreCard.Configurations;
using CreditExchange.ScoreCard.Events;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Foundation.Lookup;
using Newtonsoft.Json.Linq;

namespace CreditExchange.ScoreCard
{
    public class ScoreCardService : IScoreCardService
    {
        public ScoreCardService(IDataAttributesEngine dataAttributesEngine, IDecisionEngineService decisionEngineService, IScoreCardExecutionRepository scoreCardExecutionRepository, IEventHubClient eventHub, ScoreCardConfiguration configuration, ITenantTime tenantTime, ILookupService lookupService)
        {
            EventHub = eventHub;
            Configuration = configuration;
            TenantTime = tenantTime;
            DataAttributesEngine = dataAttributesEngine;
            DecisionEngineService = decisionEngineService;
            ScoreCardExecutionRepository = scoreCardExecutionRepository;
            LookupService = lookupService;
        }

        private IEventHubClient EventHub { get; }
        private ScoreCardConfiguration Configuration { get; }
        private ITenantTime TenantTime { get; }
        private IDataAttributesEngine DataAttributesEngine { get; }
        private IDecisionEngineService DecisionEngineService { get; }
        private IScoreCardExecutionRepository ScoreCardExecutionRepository { get; }
        private ILookupService LookupService { get; }

        public async Task<List<ScoreCardRuleResult>> RunRule(string entityType, string entityId, string productId, string ruleType, string group, string rule, Dictionary<string, string> referenceNumbers = null)
        {
            EnsureInputIsValid(entityId, productId, ruleType, group);
            entityType = EnsureEntityType(entityType);

            IEnumerable<ScoreCardDetail> scoreCardRules = await GetRuleConfiguration(entityType, productId, ruleType, group);
            IEnumerable<ScoreCardDetail> extractedScoreCardRules = new List<ScoreCardDetail>();
            if (!string.IsNullOrWhiteSpace(rule))
                extractedScoreCardRules = scoreCardRules.Where(r => r.Name == rule);

            var resultDetails = new List<ScoreCardRuleResult>();
            foreach (var scoreCardDetail in extractedScoreCardRules.OrderBy(r => r.Sequence))
            {
                var requiredSources = scoreCardDetail.RequiredSources;
                if (requiredSources == null || requiredSources.Length <= 0)
                    throw new NotFoundException($"No source found for {rule} in configuration");

                var sources = new List<string>();
                sources.AddRange(requiredSources);

                var optionalSources = scoreCardDetail.OptionalSources;
                if (optionalSources != null && optionalSources.Length > 0)
                {
                    sources.AddRange(optionalSources);
                }
                var executedRules = new List<IScoreCardRuleResult>();
                var preRequisiteRules = scoreCardDetail.PreRequisiteRules;

                if (preRequisiteRules != null && preRequisiteRules.Length > 0)
                {
                    var scoreCardResultDetail = await GetScoreCardResultDetail(entityType, entityId);
                    if (scoreCardResultDetail == null || scoreCardResultDetail.Count <= 0)
                        throw new InvalidOperationException("All pre requisite rules are not executed");

                    executedRules =
                        scoreCardResultDetail.Where(
                                sc => preRequisiteRules.Contains(sc.Name, StringComparer.InvariantCultureIgnoreCase))
                            .GroupBy(r => r.Name)
                            .Select(g => g.OrderByDescending(x => x.ExecutedDate).First())
                            .ToList();

                    if (executedRules.Count < preRequisiteRules.Length)
                        throw new InvalidOperationException("All pre requisite rules are not executed");

                    if (executedRules.Any(er => er.Result == false))
                        throw new InvalidOperationException("All pre requisite rules are not succeeded");
                }

                var attributes = await DataAttributesEngine.GetAttribute(entityType, entityId, sources);
                if (attributes == null)
                    throw new NotFoundException("No data attributes found for sources.");

                var isAllAttributes =
                    requiredSources.Any(r => attributes.Any(a => string.Equals(r, a.Key, StringComparison.InvariantCultureIgnoreCase)));
                if (!isAllAttributes)
                    throw new InvalidOperationException("Data attributes for all sources are not found");

                if (referenceNumbers != null && referenceNumbers.Any())
                {
                    foreach (var referenceNumber in referenceNumbers)
                    {
                        var source = attributes.FirstOrDefault(x => string.Equals(x.Key, referenceNumber.Key, StringComparison.InvariantCultureIgnoreCase));
                        object referenceNumberValue;

                        var innerAttributes = new CaseInsensitiveDictionary<object>(Newtonsoft.Json.Linq.JObject.FromObject(source.Value).ToObject<Dictionary<string, object>>());

                        if (!innerAttributes.TryGetValue("referenceNumber", out referenceNumberValue))
                            throw new InvalidOperationException("Reference number not found");

                        if (!string.Equals(Convert.ToString(referenceNumberValue), referenceNumber.Value, StringComparison.InvariantCultureIgnoreCase))
                            throw new InvalidOperationException("Reference number not matched");
                    }
                }

                foreach (var er in executedRules)
                {
                    var intermidiateResult = new Dictionary<string, object> { { er.Name, er.IntermediateData } };
                    attributes.Add(er.Name, intermidiateResult);
                }

                var ruleResult = ExecuteRule(attributes, scoreCardDetail);
                var executionDetail = new ScoreCardRuleResult
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ExecutedDate = TenantTime.Now,
                    IntermediateData = ruleResult.Data,
                    Name = scoreCardDetail.Name,
                    ResultDetail = ruleResult.Detail,
                    Result = ruleResult.Result,
                    ConfigurationDetail = scoreCardDetail,
                    SourceData = attributes,
                    ExceptionDetail = ruleResult.Exception
                };
                ScoreCardExecutionRepository.Add(executionDetail);
                resultDetails.Add(executionDetail);
                await EventHub.Publish(new ScoreCardRuleExecuted { ScoreCardRuleResult = executionDetail });
            }

            return resultDetails;
        }

        public async Task<List<IScoreCardRuleResult>> GetScoreCardResultDetail(string entityType, string entityId)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("entity id is required");

            var scoreCardDetail = await ScoreCardExecutionRepository.GetScorecardDetails(entityType, entityId);
            if (scoreCardDetail == null || scoreCardDetail.Count <= 0)
                throw new NotFoundException($"No score card detail found for {entityId} ");
            return scoreCardDetail;
        }

        public async Task<CaseInsensitiveDictionary<CaseInsensitiveDictionary<List<ScoreCardDetail>>>> GetRuleConfiguration(string entityType, string productId)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);

                if (Configuration == null)
                    throw new NotFoundException("Configuration is not set.");

                CaseInsensitiveDictionary<CaseInsensitiveDictionary<CaseInsensitiveDictionary<List<ScoreCardDetail>>>> entityTypeConfiguration;
                if (!Configuration.Scorecard.TryGetValue(entityType, out entityTypeConfiguration))
                    throw new NotFoundException($"The scorecard not found in configuration for {entityType}");

                CaseInsensitiveDictionary<CaseInsensitiveDictionary<List<ScoreCardDetail>>> productConfiguration;
                if (!entityTypeConfiguration.TryGetValue(productId, out productConfiguration))
                    throw new NotFoundException($"The {productId} not found for {entityType}");

                return productConfiguration;
            });
        }

        private async Task<List<ScoreCardDetail>> GetRuleConfiguration(string entityType, string productId, string ruleType, string group)
        {
            var productScoreCard = await GetRuleConfiguration(entityType, productId);

            CaseInsensitiveDictionary<List<ScoreCardDetail>> ruleTypeConfiguration;
            if (!productScoreCard.TryGetValue(ruleType, out ruleTypeConfiguration))
                throw new NotFoundException($"The {ruleType} not found for {productId}");

            List<ScoreCardDetail> groupConfiguration;
            if (!ruleTypeConfiguration.TryGetValue(group, out groupConfiguration))
                throw new NotFoundException($"The {group} not found for {ruleType}");

            return groupConfiguration;
        }

        private IScoreCardSourceResult ExecuteRule(Dictionary<string, object> data, IScoreCardDetail ruleDetail)
        {
            if (string.IsNullOrEmpty(ruleDetail.Rule?.Name) || string.IsNullOrEmpty(ruleDetail.Rule?.Version))
                throw new InvalidOperationException($"Rule is not set for {ruleDetail.Name}");

            var executionResult = DecisionEngineService.Execute<dynamic, ScoreCardSourceResult>(
                        ruleDetail.Rule.Name,
                        new { payload = data });

            return executionResult;
        }

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            if (LookupService.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");

            return entityType;
        }

        public async Task UpdateScoreCardResult(string entityType, string entityId, IScoreCardRuleResult scoreCardRuleResult)
        {
            await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);

                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentException($"{nameof(entityId)} is mandatory");

                if (scoreCardRuleResult == null)
                    throw new ArgumentException($"{nameof(scoreCardRuleResult)} is mandatory");

                ScoreCardExecutionRepository.UpdateScoreCardResult(scoreCardRuleResult);
            });
        }
        public async Task UpdateIntermediateData(string entityType, string entityId, string scoreCardId, string intermediateDataKey, string intermediateDataValue)
        {
            await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);

                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentException($"{nameof(entityId)} is mandatory");
                if (string.IsNullOrWhiteSpace(scoreCardId))
                    throw new ArgumentException($"{nameof(scoreCardId)} is mandatory");
                if (string.IsNullOrWhiteSpace(intermediateDataKey))
                    throw new ArgumentException($"{nameof(intermediateDataKey)} is mandatory");
                if (string.IsNullOrWhiteSpace(intermediateDataValue))
                    throw new ArgumentException($"{nameof(intermediateDataValue)} is mandatory");
                var scoreCardRuleResult = GetScoreCardResultDetail(entityType, entityId).Result.Where(i => i.Id == scoreCardId).LastOrDefault();
                var intermediateData = new Dictionary<string, object>();
                intermediateData.Add(intermediateDataKey, JArray.Parse(intermediateDataValue));
                scoreCardRuleResult.IntermediateData = intermediateData;
                ScoreCardExecutionRepository.UpdateScoreCardResult(scoreCardRuleResult);
            });
        }
        private void EnsureInputIsValid(string entityId, string productId, string ruleType, string group)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            if (string.IsNullOrWhiteSpace(productId))
                throw new ArgumentException($"{nameof(productId)} is mandatory");
            if (string.IsNullOrWhiteSpace(ruleType))
                throw new ArgumentException($"{nameof(ruleType)} is mandatory");
            if (string.IsNullOrWhiteSpace(group))
                throw new ArgumentException($"{nameof(group)} is mandatory");


        }
    }
}