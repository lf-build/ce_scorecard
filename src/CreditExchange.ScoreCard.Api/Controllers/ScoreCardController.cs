﻿using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreditExchange.ScoreCard.Api.Controllers
{
    [Route("/")]
    public class ScoreCardController : ExtendedController
    {
        public ScoreCardController(IScoreCardService scoreCardSyndicationService)
        {
            if (scoreCardSyndicationService == null)
                throw new ArgumentNullException(nameof(scoreCardSyndicationService));

            ScoreCardSyndicationService = scoreCardSyndicationService;
        }

        private IScoreCardService ScoreCardSyndicationService { get; }

        [HttpGet]
        public IActionResult Getall(string entityType, string productId)
        {
            return Execute(() => Ok(new ScoreCardSourceResult
            {
                Data = new Dictionary<string, object>(),
                Detail = new List<string> { "test1", "test" },
                Result = true
            }));
        }

        [HttpGet("{entityType}/{productId}/configurations")]
        [ProducesResponseType(typeof(CaseInsensitiveDictionary<CaseInsensitiveDictionary<List<IScoreCardDetail>>>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetScoreCardConfiguation(string entityType, string productId)
        {
            return await ExecuteAsync(async () => Ok(await ScoreCardSyndicationService.GetRuleConfiguration(entityType, productId)));
        }

        [HttpPost("{entityType}/{entityId}/{productId}/{ruleType}/{group}/{rule?}")]
        [ProducesResponseType(typeof(List<ScoreCardRuleResult>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]

        public async Task<IActionResult> RunRule(string entityType, string entityId, string productId, string ruleType, string group, string rule, [FromBody] Dictionary<string, string> referenceNumbers)
        {
            return await ExecuteAsync(async () => Ok(await ScoreCardSyndicationService.RunRule(entityType, entityId, productId, ruleType, @group, rule, referenceNumbers)));
        }

        [HttpGet("{entityType}/{entityId}")]
        [ProducesResponseType(typeof(List<IScoreCardRuleResult>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetScoreCardResultDetail(string entityType, string entityId)
        {
            return await ExecuteAsync(async () => Ok(await ScoreCardSyndicationService.GetScoreCardResultDetail(entityType, entityId)));
        }

        [HttpPost("{entityType}/{entityId}/updatescorecardresult")]
        [ProducesResponseType(typeof(IScoreCardRuleResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateScoreCardResult(string entityType, string entityId, ScoreCardRuleResult scoreCardRuleResult)
        {
            return await ExecuteAsync(async () =>
            {
                await ScoreCardSyndicationService.UpdateScoreCardResult(entityType, entityId, scoreCardRuleResult);
                return Ok();
            });
        }
        [HttpPost("{entityType}/{entityId}/updateintermediatedata/{scoreCardId}")]
        [ProducesResponseType(typeof(IScoreCardRuleResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateIntermediatedata(string entityType, string entityId, string scoreCardId, string intermediateDataKey, string intermediateDataValue)
        {
            return await ExecuteAsync(async () =>
            {
                await ScoreCardSyndicationService.UpdateIntermediateData(entityType, entityId, scoreCardId, intermediateDataKey, intermediateDataValue);
                return Ok();
            });
        }
    }
}
