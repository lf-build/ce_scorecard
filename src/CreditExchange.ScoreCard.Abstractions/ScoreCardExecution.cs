﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using CreditExchange.ScoreCard.Configurations;

namespace CreditExchange.ScoreCard
{
    public class ScoreCardRuleResult : Aggregate, IScoreCardRuleResult
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string Name { get; set; }
        public DateTimeOffset ExecutedDate { get; set; }
        public bool Result { get; set; }
        public List<string> ResultDetail { get; set; }
        public object IntermediateData { get; set; }
        public ScoreCardDetail ConfigurationDetail { get; set; }
        public Dictionary<string, object> SourceData { get; set; }
        public List<string> ExceptionDetail { get; set; }
    }
}
