﻿using System;

namespace CreditExchange.ScoreCard
{
    public  interface IRuleExecutionData
    {
        string Name { get; set; }
        string TypeOfRule { get; set; }
        object Data { get; set; }

        DateTimeOffset ExecutionDate { get; set; }
    }
}
