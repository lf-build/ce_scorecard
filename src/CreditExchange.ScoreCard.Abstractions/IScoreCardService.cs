﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CreditExchange.ScoreCard.Configurations;

namespace CreditExchange.ScoreCard
{
    public interface IScoreCardService
    {
        Task<CaseInsensitiveDictionary<CaseInsensitiveDictionary<List<ScoreCardDetail>>>> GetRuleConfiguration(string entityType, string productId);

        Task<List<ScoreCardRuleResult>> RunRule(string entityType, string entityId, string productId, string ruleType,
                                                string @group, string rule = null, Dictionary<string, string> referenceNumbers = null);

        Task<List<IScoreCardRuleResult>> GetScoreCardResultDetail(string entityType, string entityId);
        Task UpdateScoreCardResult(string entityType, string entityId, IScoreCardRuleResult scoreCardRuleResult);

        Task UpdateIntermediateData(string entityType, string entityId, string scoreCardId,string intermediateDataKey, string intermediateDataValue);

    }
}
