﻿namespace CreditExchange.ScoreCard.Events
{
    public class ScoreCardRuleExecuted
    {
        public IScoreCardRuleResult ScoreCardRuleResult { get; set; }
    }
}
