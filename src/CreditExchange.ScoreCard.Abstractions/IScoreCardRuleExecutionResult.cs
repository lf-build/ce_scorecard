﻿using System.Collections.Generic;

namespace CreditExchange.ScoreCard
{
    public interface IScoreCardSourceResult
    {
        Dictionary<string, object> Data { get; set; }
        List<string> Detail { get; set; }
        bool Result { get; set; }
         List<string> Exception { get; set; }
         string RejectCode { get; set; }
    }
}
