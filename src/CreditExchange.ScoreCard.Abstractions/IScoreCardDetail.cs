﻿using CreditExchange.ScoreCard.Configurations;

namespace CreditExchange.ScoreCard
{
    public interface IScoreCardDetail
    {
         string Name { get; set; }
         int Sequence { get; set; }
         string[] RequiredSources { get; set; }
         Rule Rule { get; set; }
         string[] PreRequisiteRules { get; set; }
    }
}