﻿namespace CreditExchange.ScoreCard.Configurations
{
    public class ScoreCardDetail : IScoreCardDetail
    {
        public string Name { get; set; }
        public int Sequence { get; set; }
        public string[] RequiredSources { get; set; }
        public string[] OptionalSources { get; set; }
        public Rule Rule { get; set; }
        public string[] PreRequisiteRules { get; set; }
}
}
