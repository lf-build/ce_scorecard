﻿namespace CreditExchange.ScoreCard.Configurations
{
    public class Rule
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }
}
