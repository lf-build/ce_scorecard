﻿using System.Collections.Generic;

namespace CreditExchange.ScoreCard.Configurations
{
    public class ScoreCardConfiguration
    {
        public CaseInsensitiveDictionary< CaseInsensitiveDictionary<CaseInsensitiveDictionary<CaseInsensitiveDictionary< List<ScoreCardDetail>>>>> Scorecard { get; set; }
    }
}
