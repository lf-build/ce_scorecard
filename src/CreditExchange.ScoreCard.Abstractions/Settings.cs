﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.ScoreCard
{
    public static class Settings
    {
        public static string ServiceName { get; } = "scorecard";

        private static string Prefix { get; } = ServiceName.ToUpper();

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "scorecard");

        public static ServiceSettings DataAttributes { get; } = new ServiceSettings($"{Prefix}_DATAATTRIBUTES_HOST", "data-attributes", $"{Prefix}_DATAATTRIBUTES_PORT");

        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings($"{Prefix}_DECISIONENGINE_HOST", "decision-engine-runtime", $"{Prefix}_DECISIONENGINE_PORT");

        public static ServiceSettings LookupService { get; } = new ServiceSettings($"{Prefix}_LOOKUPSERVICE", "lookup-service");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}
