﻿using System.Collections.Generic;

namespace CreditExchange.ScoreCard
{
    public class ScoreCardSourceResult : IScoreCardSourceResult
    {
        public Dictionary<string, object> Data { get; set; }
        public List<string> Detail { get; set; }
        public bool Result { get; set; }
        public List<string> Exception { get; set; }
        public string RejectCode { get; set; }
    }
}
