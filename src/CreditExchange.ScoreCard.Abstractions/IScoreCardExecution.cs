﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using CreditExchange.ScoreCard.Configurations;

namespace CreditExchange.ScoreCard
{
    public interface IScoreCardRuleResult : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        DateTimeOffset ExecutedDate { get; set; }
        object IntermediateData { get; set; }
        string Name { get; set; }
        bool Result { get; set; }
        List<string> ResultDetail { get; set; }
        ScoreCardDetail ConfigurationDetail { get; set; }
        Dictionary<string, object> SourceData { get; set; }
        List<string> ExceptionDetail { get; set; }
    }
}