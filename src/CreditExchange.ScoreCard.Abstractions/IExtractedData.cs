﻿using System;

namespace CreditExchange.ScoreCard
{
    public interface IExtractedData
    {
        string Name { get; set; }
        DateTimeOffset ExtractedDate { get; set; }
        object Data { get; set; }
    }
}
