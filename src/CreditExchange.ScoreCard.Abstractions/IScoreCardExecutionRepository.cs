﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreditExchange.ScoreCard
{
    public interface IScoreCardExecutionRepository : IRepository<IScoreCardRuleResult>
    {
        Task<List<IScoreCardRuleResult>> GetScorecardDetails(string entityType, string entityId);

        Task<IScoreCardRuleResult> UpdateScoreCardResult(IScoreCardRuleResult scoreCardRuleResult);
    }
}
