﻿using CreditExchange.ScoreCard.Configurations;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreditExchange.ScoreCard.Persistence
{
    public class JsonSerializer<T> : IBsonSerializer<object>
    {
        public object Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var jsonString = context.Reader.ReadString();
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
        {
            var valueAsJson = JsonConvert.SerializeObject(value);
            context.Writer.WriteString(valueAsJson);
        }

        public Type ValueType => typeof(T);
    }
    public class ScoreCardExecutionRepository : MongoRepository<IScoreCardRuleResult, ScoreCardRuleResult>, IScoreCardExecutionRepository
    {
        static ScoreCardExecutionRepository()
        {
            BsonClassMap.RegisterClassMap<ScoreCardRuleResult>(map =>
            {
                map.AutoMap();

                map.MapProperty(s => s.ExecutedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapProperty(s => s.IntermediateData).SetSerializer(new JsonSerializer<object>());
                map.MapProperty(s => s.SourceData).SetSerializer(new JsonSerializer<Dictionary<string, object>>());
                var type = typeof(ScoreCardRuleResult);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<ScoreCardDetail>(map =>
            {
                map.AutoMap();
                var type = typeof(ScoreCardDetail);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<Rule>(map =>
            {
                map.AutoMap();
                var type = typeof(Rule);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }
        public ScoreCardExecutionRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "scorecardruleresult")
        {
            CreateIndexIfNotExists("ruleresult_id",
                Builders<IScoreCardRuleResult>.IndexKeys.Ascending(i => i.EntityType)
                    .Ascending(i => i.EntityId));
        }

        public async Task<List<IScoreCardRuleResult>> GetScorecardDetails(string entityType, string entityId)
        {
            return await Query.Where(sc => sc.EntityType == entityType && sc.EntityId == entityId).ToListAsync();
        }

        public async Task<IScoreCardRuleResult> UpdateScoreCardResult(IScoreCardRuleResult scoreCardRuleResult)
        {

            await Collection.UpdateOneAsync(s =>
                    s.TenantId == TenantService.Current.Id &&
                    s.EntityType == scoreCardRuleResult.EntityType &&
                    s.EntityId == scoreCardRuleResult.EntityId &&
                    s.Name == scoreCardRuleResult.Name &&
                    s.Id == scoreCardRuleResult.Id,
                new UpdateDefinitionBuilder<IScoreCardRuleResult>()
                    .Set(a => a.IntermediateData, scoreCardRuleResult.IntermediateData)
                , new UpdateOptions() { IsUpsert = true });

            return scoreCardRuleResult;
        }

        private FilterDefinition<IScoreCardRuleResult> GetByScoreCardId(IScoreCardRuleResult scoreCardRuleResult)
        {
            return Builders<IScoreCardRuleResult>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                          a.EntityId == scoreCardRuleResult.EntityId && a.Id == scoreCardRuleResult.Id);
        }
    }
}
