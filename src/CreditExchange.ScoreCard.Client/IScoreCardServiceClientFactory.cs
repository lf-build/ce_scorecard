﻿using LendFoundry.Security.Tokens;

namespace CreditExchange.ScoreCard.Client
{
    /// <summary>
    /// ScoreCard Service Client Factory interface
    /// </summary>
    public interface IScoreCardServiceClientFactory
    {
        /// <summary>
        /// Creates the specified reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns></returns>
        IScoreCardService Create(ITokenReader reader);
    }
}
