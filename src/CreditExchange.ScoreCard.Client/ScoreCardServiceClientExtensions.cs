﻿using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
namespace CreditExchange.ScoreCard.Client
{
    /// <summary>
    /// Geo Profile Service Client Extensions class
    /// </summary>
    public static class ScoreCardServiceClientExtensions
    {
        /// <summary>
        /// Adds the application service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="port">The port.</param>
        /// <returns>Return service collection</returns>
        public static IServiceCollection AddScoreCardService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IScoreCardServiceClientFactory>(p => new ScoreCardServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IScoreCardServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
