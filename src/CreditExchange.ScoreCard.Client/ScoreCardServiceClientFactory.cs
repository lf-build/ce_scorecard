﻿using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Client;

namespace CreditExchange.ScoreCard.Client
{
    public class ScoreCardServiceClientFactory : IScoreCardServiceClientFactory
    {
        public ScoreCardServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        #region Private Properties
        private IServiceProvider Provider { get; }
        private string Endpoint { get; }
        private int Port { get; }

        #endregion

        #region Public Methods
        public IScoreCardService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new ScoreCardService(client);
        }
        #endregion
    }
}
