﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using CreditExchange.ScoreCard.Configurations;

namespace CreditExchange.ScoreCard.Client
{
    /// <summary>
    /// ScoreCard Service class
    /// </summary>
    public class ScoreCardService : IScoreCardService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScoreCardService"/> class.
        /// </summary>
        /// <param name="client">The client.</param>
        public ScoreCardService(IServiceClient client)
        {
            Client = client;
        }

        /// <summary>
        /// Gets the client.
        /// </summary>
        /// <value>
        /// The client.
        /// </value>
        private IServiceClient Client { get; }

        /// <summary>
        /// Get /Verify the pincode
        /// </summary>
        /// <returns>GeoPincodeResponse object</returns>
        

        public async Task<CaseInsensitiveDictionary<CaseInsensitiveDictionary<List<ScoreCardDetail>>>> GetRuleConfiguration(string entityType, string productId)
        {
            var request = new RestRequest("{entityType}/{productId}/configurations", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("productId", productId);
            var result = await Client.ExecuteAsync<CaseInsensitiveDictionary<CaseInsensitiveDictionary<List<ScoreCardDetail>>>>(request);            
            return new CaseInsensitiveDictionary<CaseInsensitiveDictionary<List<ScoreCardDetail>>>(result);
        }

        public async Task<List<IScoreCardRuleResult>> GetScoreCardResultDetail(string entityType, string entityId)
        {
            var request = new RestRequest("{entityType}/{entityId}", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            var result = await Client.ExecuteAsync<List<ScoreCardRuleResult>>(request);
            return new List<IScoreCardRuleResult>(result);
        }
        public async Task UpdateScoreCardResult(string entityType, string entityId, IScoreCardRuleResult scoreCardRuleResult)
        {
            var request = new RestRequest("{entityType}/{entityId}/updatescorecardresult", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(scoreCardRuleResult);
            await Client.ExecuteAsync(request);
        }

        public async Task<List<ScoreCardRuleResult>> RunRule(string entityType, string entityId, string productId, string ruleType, string @group, string rule = null, Dictionary<string, string> referenceNumbers = null)
        {
            var request = new RestRequest("{entityType}/{entityId}/{productId}/{ruleType}/{group}/{rule}", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("productId", productId);
            request.AddUrlSegment("ruleType", ruleType);
            request.AddUrlSegment("group", group);
            request.AddUrlSegment("rule", rule);
            if(referenceNumbers != null)
            {
                request.AddJsonBody(referenceNumbers);
            }else
            {
                request.AddJsonBody(new Dictionary<string, string>());
            }
            return await Client.ExecuteAsync<List<ScoreCardRuleResult>>(request);
        }

         public async Task UpdateIntermediateData(string entityType, string entityId, string scoreCardId, string intermediateDataKey, string intermediateDataValue)
        {
            var request = new RestRequest("{entityType}/{entityId}/updateintermediatedata/{scoreCardId}", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("scoreCardId", scoreCardId);
            request.AddParameter("intermediateDataKey", intermediateDataKey);
            request.AddParameter("intermediateDataValue", intermediateDataValue);
            await Client.ExecuteAsync(request);
        }
    }
}
